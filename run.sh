#!/bin/bash
## Options.
# BENCHMARKS_DIR - Directory where benchmark executables are stored.
# OUT_DIR - Directory where results should be stored.
# EIGEN_DIR - Where to find Eigen.
# BENCHMARK_FILTER - Which filter to use, if any, for the benchmarks.
# BID - The ID to use on this benchmark result or a random number otherwise.
unset EIGEN_DIR BENCHMARK_FILTER BID BENCHMARKS_DIR OUT_DIR

usage()
{
    echo "Usage: $0 [ -b BENCHMARKS_DIR ] [ -o OUT_DIR ] [ -e EIGEN_DIR ] [ -f BENCHMARK_FILTER ] [ -i BID ]"
    exit 2
}

while getopts 'b:o:e:f:i:' opt
do
    case $opt in
        b) BENCHMARKS_DIR=$OPTARG ;;
        o) OUT_DIR=$OPTARG ;;
        e) EIGEN_DIR=$OPTARG ;;
        f) BENCHMARK_FILTER=$OPTARG ;;
        i) BID=$OPTARG ;;
    esac
done

[ -z $BENCHMARKS_DIR ] && usage
[ -z $OUT_DIR ] && usage
[ -z $EIGEN_DIR ] && usage
[ -z $BID ] && BID=$(echo $RANDOM)

## Internal variables
CUR_DIR=$(pwd)
DATE=$(date -u +%Y%m%d%H%m)
CPU=$(lscpu | awk -F ":"  '/Architecture/ {gsub(/ /,""); print $2; exit}')
MEMORY=$(lshw -c memory 2> /dev/null | awk -F ":" '/size/ {gsub(/ /,""); print $2}')

## Get commit information
cd $EIGEN_DIR
echo "Using Eigen at " $(pwd)
COMMIT_TMP=$(git log -n 1 --oneline --no-decorate --pretty=format:'%h %as' | awk '{gsub(/-/,""); print}')
COMMIT=$(echo $COMMIT_TMP | awk '{print $1}')
COMMIT_DATE=$(echo $COMMIT_TMP | awk '{print $2}')
cd $CUR_DIR

#Sometimes git %as does not work...
if [[ $COMMIT_DATE =~ .*"%as".* ]]; then
    COMMIT_DATE="00000000"
fi
SUFFIX="$DATE"_"$COMMIT"_"$COMMIT_DATE"_"$BID"_"$CPU"

echo $SUFFIX

#Comile benchmarks
if [[ ! -e $BENCHMARKS_DIR ]]; then
    mkdir -p $BENCHMARKS_DIR
    cd $BENCHMARKS_DIR
    cmake -DEigen3_DIR=$EIGEN_DIR -DCMAKE_BUILD_TYPE=Release -DBENCHMARK_ENABLE_GTEST_TESTS=OFF ..
    cmake --build .
else
    cd $BENCHMARKS_DIR
    rm -f gemm
    cmake --build . -- gemm
fi
cd $CUR_DIR

## Run benchmarks
OUT_DIR_FINAL=$OUT_DIR/$CPU/$DATE
mkdir -p $OUT_DIR_FINAL
if [[ -z $BENCHMARK_FILTER ]]; then
    echo "Starting execution"
else
    echo "Starting execution with filters $BENCHMARK_FILTER"
fi

$BENCHMARKS_DIR/gemm --benchmark_format=json --benchmark_filter=$BENCHMARK_FILTER > $OUT_DIR_FINAL/gemm_$SUFFIX.json

echo "Storing results"
## Store results and info
cd $OUT_DIR_FINAL
echo "Benchmark ID: $BID" > info_$SUFFIX
echo "Date: $DATE" >> info_$SUFFIX
echo "Memory: $MEMORY" >> info_$SUFFIX
echo "Commit: $COMMIT" >> info_$SUFFIX
lscpu >> info_$SUFFIX
