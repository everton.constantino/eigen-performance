#!/bin/bash
CUR_DIR=$(pwd)
COMMIT_COUNT=0

unset EIGEN_URL FROM_YEAR TO_YEAR MAX_COMMITS EIGEN_CLONE_DIR EIGEN_BRANCH

usage()
{
    echo "Usage: $0 [ -u EIGEN_URL ] [ -f FROM_YEAR ] [ -t TO_YEAR ] [ -m MAX_COMMITS ] [ -c EIGEN_CLONE_DIR ] [ -B EIGEN_BRANCH ]"
    exit 2
}

while getopts 'u:f:t:m:B:' opt
do
    case $opt in
        u) EIGEN_URL=$OPTARG ;;
        f) COMMIT_LIST_FILE=$OPTARG ;;
        t) TO_YEAR=$OPTARG ;;
        m) MAX_COMMITS=$OPTARG ;;
        c) EIGEN_CLONE_DIR=$OPTARG ;;
        B) EIGEN_BRANCH=$OPTARG ;;
    esac
done

[ -z $EIGEN_URL       ] && EIGEN_URL="https://gitlab.com/libeigen/eigen.git"
[ -z $FROM_YEAR       ] && FROM_YEAR=2019
[ -z $TO_YEAR         ] && TO_YEAR=$(date +%Y)
[ -z $EIGEN_CLONE_DIR ] && EIGEN_CLONE_DIR=$CUR_DIR/tmp
[ -z $EIGEN_BRANCH    ] && EIGEN_BRANCH="master"

if [[ ! -e $EIGEN_CLONE_DIR ]]; then 
    mkdir -p $EIGEN_CLONE_DIR
fi

cd $EIGEN_CLONE_DIR

if [[ ! -e eigen ]]; then 
    git clone -b $EIGEN_BRANCH $EIGEN_URL &> /dev/null
fi

cd eigen
git checkout $EIGEN_BRANCH &> /dev/null
git pull origin $EIGEN_BRANCH &> /dev/null
EIGEN_DIR=$(pwd)

echo "Eigen dir " $EIGEN_DIR 1>&2

for ((YEAR = $TO_YEAR; YEAR >= FROM_YEAR; YEAR--)); do
    for ((MONTH = 12; MONTH >= 1; MONTH--)); do
        TYEAR=$YEAR
        NMONTH=$((MONTH + 1))
        if [[ $NMONTH -gt 12 ]]; then
            NMONTH=1
            TYEAR=$((YEAR + 1))
        fi

        AFTER=$YEAR/$MONTH/1
        BEFORE=$TYEAR/$NMONTH/1
        COMMIT=$(git log -n 1 --after=$AFTER --before=$BEFORE --no-decorate --pretty=format:'%h %as' | awk '{gsub(/-/,""); print $1}')
        if [[ ! -z $COMMIT ]]; then
            echo $COMMIT
            COMMIT_COUNT=$((COMMIT_COUNT + 1))
            if [[ ! -z $MAX_COMMITS ]]; then
                if [[ COMMIT_COUNT -ge MAX_COMMITS ]]; then
                    exit
                fi
            fi
        fi
    done
done