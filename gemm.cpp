#include "util.hpp"

#ifdef USE_OPENBLAS
template <typename MatrixType, long N>
void BM_GEMM_Fixed_Square(benchmark::State& state)
{
  for(auto k = 0; k < state.range(0); k++)
  {
    MatrixType A[N][N];
    MatrixType B[N][N];
    MatrixType C[N][N];

    for(auto i = 0; i < N; i++)
    {
      for(auto j = 0; j < N; j++)
      {
        A[i][j] = rand();
        B[i][j] = rand();
      }
    }
    for (auto _ : state) {
      gemm<MatrixType>(CblasColMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1, (MatrixType *)A, N, (MatrixType *)B, N, 0, (MatrixType *)C, N);
    }
  }
}
template <typename MatrixType>
void BM_GEMM_Dynamic_Square(benchmark::State& state)
{
  long N = state.range(0);
  MatrixType *A = new MatrixType[N*N];
  MatrixType *B = new MatrixType[N*N];
  MatrixType *C = new MatrixType[N*N];

  for(auto i = 0; i < N; i++)
  {
    for(auto j = 0; j < N; j++)
    {
      A[i*N + j] = rand();
      B[i*N + j] = rand();
    }
  }

  for (auto _ : state) {
    gemm<MatrixType>(CblasColMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1, (MatrixType *)A, N, (MatrixType *)B, N, 0, (MatrixType *)C, N);
  }
}
template <typename MatrixType>
void BM_GEMM_Dynamic(benchmark::State& state)
{
  long M = state.range(0), K = state.range(1), N = state.range(2);
  MatrixType *A = new MatrixType[M*K];
  MatrixType *B = new MatrixType[K*N];
  MatrixType *C = new MatrixType[M*N];

  for(auto i = 0; i < K; i++)
  {
    for(auto j = 0; j < M; j++)
    {
      A[i*K + j] = rand();
    }
    for(auto j = 0; j < N; j++)
    {
      B[i*N + j] = rand();
    }
  }

  for (auto _ : state) {
    gemm<MatrixType>(CblasColMajor, CblasNoTrans, CblasNoTrans, M, N, K, 1, (MatrixType *)A, M, (MatrixType *)B, K, 0, (MatrixType *)C, M);
  }
}
#else
template <typename MatrixType, long N>
void BM_GEMM_Fixed_Square(benchmark::State& state)
{
  typedef Eigen::Matrix<MatrixType, N, N> Matrix;
  for(auto k = 0; k < state.range(0); k++)
  {
    Matrix A = Matrix::Random(N, N);
    Matrix B = Matrix::Random(N, N);
    Matrix C;
    for (auto _ : state) {
      C.noalias() = A * B;
    }
  }
}
template <typename MatrixType>
void BM_GEMM_Dynamic_Square(benchmark::State& state)
{
  typedef Eigen::Matrix<MatrixType, Eigen::Dynamic, Eigen::Dynamic> Matrix;
  Matrix A = Matrix::Random(state.range(0), state.range(0));
  Matrix B = Matrix::Random(state.range(0), state.range(0));
  Matrix C;
  for (auto _ : state) {
    C.noalias() = A * B;
  }
}
template <typename MatrixType>
void BM_GEMM_Dynamic(benchmark::State& state)
{
  typedef Eigen::Matrix<MatrixType, Eigen::Dynamic, Eigen::Dynamic> Matrix;
  Matrix A = Matrix::Random(state.range(0), state.range(1));
  Matrix B = Matrix::Random(state.range(1), state.range(2));
  Matrix C;
  for (auto _ : state) {
    C.noalias() = A * B;
  }
}
#endif

// Register the function as a benchmark
#define ADD_BENCH(S,K)  BENCHMARK_TEMPLATE(BM_GEMM_Fixed_Square, \
                                           float, S)->Arg(K); \
                        BENCHMARK_TEMPLATE(BM_GEMM_Fixed_Square, \
                                           double, S)->Arg(K);

ADD_BENCH(3,500000);
ADD_BENCH(4,500000);
ADD_BENCH(5,500000);
ADD_BENCH(6,50000);
ADD_BENCH(7,50000);
ADD_BENCH(8,50000);
ADD_BENCH(9,50000);
ADD_BENCH(10,50000);
ADD_BENCH(11,50000);
ADD_BENCH(12,50000);
ADD_BENCH(13,50000);
ADD_BENCH(14,50000);
ADD_BENCH(15,5000);
ADD_BENCH(16,5000);
ADD_BENCH(32,5000);
ADD_BENCH(64,500);
ADD_BENCH(128,500);

BENCHMARK_TEMPLATE(BM_GEMM_Dynamic_Square, float)->RangeMultiplier(2)->Range(2,4096);
BENCHMARK_TEMPLATE(BM_GEMM_Dynamic_Square, double)->RangeMultiplier(2)->Range(2,4096);

BENCHMARK_TEMPLATE(BM_GEMM_Dynamic, float)->RangeMultiplier(4)->Ranges({{256,4096},{256,4096},{256,4096}});
BENCHMARK_TEMPLATE(BM_GEMM_Dynamic, double)->RangeMultiplier(4)->Ranges({{256,4096},{256,4096},{256,4096}});

// Run the benchmark
BENCHMARK_MAIN();
