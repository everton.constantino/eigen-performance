#!python
import http.client
import json
import os
import argparse
from datetime import datetime

pipelineId = None
projectId = None
privateToken = None

d = datetime.now()
tmpDir = 'tmp{}{}{}{}{}{}'.format(d.year, d.month, d.day, d.hour, d.minute, d.second)

artifacts = []

description = 'Extract GitLab pipeline artifacts.'

parser = argparse.ArgumentParser(description=description)
parser.add_argument('--project', help='Project ID.', required=True)
parser.add_argument('--pipeline', help='Pipeline ID.', required=True)
parser.add_argument('--token', help='Access Token.', required=True)
parser.add_argument('--dir', help='Output store directory.', required=False)
args = parser.parse_args()

if args.project:
    projectId = args.project

if args.pipeline:
    pipelineId = args.pipeline

if args.token:
    privateToken = args.token

if args.dir:
    tmpDir = args.dir

h = http.client.HTTPSConnection('gitlab.com')
h.request('GET','/api/v4/projects/{}/pipelines/{}/jobs'.format(projectId, pipelineId),None,{'Content-Type':'application/json','PRIVATE-TOKEN':privateToken})
r = h.getresponse()
jobsJson = json.loads(r.read())
h.close()
for job in jobsJson:
    if 'status' in job and job['status'] == 'success':
        artifacts.append('https://gitlab.com/api/v4/projects/{}/jobs/{}/artifacts/'.format(projectId, job['id']))

for artifact in artifacts:
    cmd = 'mkdir {0};cd {0};wget -v --header="PRIVATE-TOKEN: {1}" {2} -O tmp.zip;unzip tmp.zip;rm tmp.zip'.format(tmpDir, privateToken, artifact)
    os.system(cmd)


