#include <benchmark/benchmark.h>
#include "config.h"
#ifdef USE_OPENBLAS
#include <cblas.h>
#include <cstdlib>
#else
#include <Eigen/Dense>
#endif

#ifdef USE_OPENBLAS
template <typename MatrixType = double>
void gemm(OPENBLAS_CONST enum CBLAS_ORDER Order, 
          OPENBLAS_CONST enum CBLAS_TRANSPOSE TransA,
          OPENBLAS_CONST enum CBLAS_TRANSPOSE TransB,
          OPENBLAS_CONST blasint M, 
          OPENBLAS_CONST blasint N, 
          OPENBLAS_CONST blasint K,
		      OPENBLAS_CONST MatrixType alpha,
          OPENBLAS_CONST MatrixType *A,
          OPENBLAS_CONST blasint lda,
          OPENBLAS_CONST MatrixType *B,
          OPENBLAS_CONST blasint ldb,
          OPENBLAS_CONST MatrixType beta,
          MatrixType *C,
          OPENBLAS_CONST blasint ldc)
{
  cblas_dgemm(Order, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}
template <>
void gemm<float>(OPENBLAS_CONST enum CBLAS_ORDER Order, 
          OPENBLAS_CONST enum CBLAS_TRANSPOSE TransA,
          OPENBLAS_CONST enum CBLAS_TRANSPOSE TransB,
          OPENBLAS_CONST blasint M, 
          OPENBLAS_CONST blasint N, 
          OPENBLAS_CONST blasint K,
		      OPENBLAS_CONST float alpha,
          OPENBLAS_CONST float *A,
          OPENBLAS_CONST blasint lda,
          OPENBLAS_CONST float *B,
          OPENBLAS_CONST blasint ldb,
          OPENBLAS_CONST float beta,
          float *C,
          OPENBLAS_CONST blasint ldc)
{
  cblas_sgemm(Order, TransA, TransB, M, N, K, alpha, A, lda, B, ldb, beta, C, ldc);
}
#endif